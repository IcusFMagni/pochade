import React, { Component } from 'react';
import logo from './logo.png';
import './header.css';

class Header extends Component {
  render() {
    return (
      <header className="Main-header">
        <img src={logo} className="Header-logo" alt="logo" />
        <h1 className="App-title">Welcome to Pochade</h1>
      </header>
    );
  }
}

export default Header;
